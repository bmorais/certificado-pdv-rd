package br.com.rd.certificadord;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

import br.com.rd.certificadord.business.FilialBusiness;
import br.com.rd.certificadord.business.impl.FilialBusinessImpl;
import br.com.rd.certificadord.utils.ServerUtil;
import br.com.rd.certificadord.utils.VerificarChaveThread;
import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public final class MainCertificado {

	private static final Logger logger = Logger.getLogger(MainCertificado.class);
	private static final String DIR_CERTIFICADO = "/pdv/NFCeClient/certificado/CertificadoA1_2025.pfx";
	
	static Semaphore semaphore = new Semaphore(5);

	public static void main(String[] args) {
		
		Filter filter = new Filter();
		
//		filter.setSgEstado("RJ");
//		filter.setCdFilial(2034);
//		filter.setNrCaixa(5);
//		filter.setCdFiliais(Arrays.asList(2034,171,34,1762,1440,3684,1348));
		filter.setSgEstados(Arrays.asList("AC","AL","AM","AP","BA",
				"DF","ES","GO","MA","MG","MS"));
		filter.setOrderByDesc(Boolean.TRUE);

		try {
			
			FilialBusiness filialBusiness = new FilialBusinessImpl();

			logger.info("BUSCANDO AS FILIAIS");

			List<FilialVO> filiais = filialBusiness.buscarFiliais(filter);
			
			logger.info("QTD - " + filiais.size());
			
			List<List<FilialVO>> servidores = ServerUtil.separarPorServidor(10, filiais);
			
			for(List<FilialVO> servidor : servidores) {
				
				Thread thread = new Thread(() -> {
		            try {
		                semaphore.acquire();
		                new VerificarChaveThread(servidor, filter, null, DIR_CERTIFICADO).run();
		            } catch (InterruptedException e) {
		            	Thread.currentThread().interrupt();
		                e.printStackTrace();
		            } finally {
		                semaphore.release();
		            }
		        });
		        
		        thread.start();
			}
			

			logger.info("SEPARADO ENTRE OS SERVERS COM SUCESSO!");

		} catch (Exception e1) {
			logger.error("Erro geral: " + e1.getMessage());
		}
	
	}
}
