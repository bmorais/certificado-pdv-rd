package br.com.rd.certificadord.business;

import java.util.List;

public interface FactoryBusiness {

	void update(List<String> queries, String dsIp) throws Exception;
	boolean insereCertificado(String file, String dsIp);
	
}
