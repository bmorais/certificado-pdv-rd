package br.com.rd.certificadord.business;

import java.util.List;

import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public interface FilialBusiness {

	public List<FilialVO> buscarFiliais(Filter filter) throws Exception;

}
