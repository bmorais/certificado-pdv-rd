package br.com.rd.certificadord.business.impl;

import java.sql.Connection;
import java.util.List;

import br.com.rd.certificadord.business.FactoryBusiness;
import br.com.rd.certificadord.dao.FactoryDAO;
import br.com.rd.certificadord.factories.ConnectionFactory;
import br.com.rd.certificadord.factories.Database;

public class FactoryBusinessImpl implements FactoryBusiness {

	FactoryDAO factoryDAO = new FactoryDAOImpl();
	
	@Override
	public void update(List<String> queries, String dsIp) throws Exception {

		try (Connection conn = ConnectionFactory.getConnectionByIp(dsIp, Database.PDV)){
			for (String query : queries) {
				factoryDAO.update(conn, query);
			}
		} catch (Exception e) {
			throw new Exception("Erro ao executar a query.. " + e.getMessage());
		}

	}

	@Override
	public boolean insereCertificado(String file,String dsIp) {
		return factoryDAO.certificado(file, dsIp);
	}

}
