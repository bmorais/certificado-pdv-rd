package br.com.rd.certificadord.business.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

import br.com.rd.certificadord.dao.FactoryDAO;
import br.com.rd.certificadord.factories.ConnectionFactory;
import br.com.rd.certificadord.factories.Database;

public class FactoryDAOImpl implements FactoryDAO {
	
	private static final String SENHA_CERTIFICADO = "49bZc5grvBCJBf4Z/BNYBA==";
	private static final Logger logger = Logger.getLogger(FactoryDAOImpl.class);

	@Override
	public void update(Connection conn, String query) throws Exception {

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			ps.executeUpdate();
		} catch (Exception e) {
			throw new Exception(e);
		}

	}

	@Override
	public boolean certificado(String file, String dsIp) {
		
		StringBuilder qry = new StringBuilder();
		
		qry.append("UPDATE ");
		qry.append(" TB_PDV_CONFIGURACAO SET ");
		qry.append(" DS_CERTIFICADO = ? ,");
		qry.append(" DS_CERTIFICADO_SENHA = ? ");
		
		try (Connection conn = ConnectionFactory.getConnectionByIp(dsIp, Database.PDV);
				PreparedStatement ps = conn.prepareStatement(qry.toString());
				InputStream cert = new FileInputStream(new File(file))) {
			
			ps.setBinaryStream(1, cert);
			ps.setString(2, SENHA_CERTIFICADO);
			
			 return ps.executeUpdate() > 0;
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}
}
