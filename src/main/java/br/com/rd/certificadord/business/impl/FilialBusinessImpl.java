package br.com.rd.certificadord.business.impl;

import java.util.List;

import br.com.rd.certificadord.business.FilialBusiness;
import br.com.rd.certificadord.dao.FilialDAO;
import br.com.rd.certificadord.dao.impl.FilialDAOImpl;
import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public class FilialBusinessImpl implements FilialBusiness {

	private FilialDAO filialDAO = new FilialDAOImpl();

	public List<FilialVO> buscarFiliais(Filter filter) throws Exception {
		return filialDAO.buscarFiliais(filter);
	}

}
