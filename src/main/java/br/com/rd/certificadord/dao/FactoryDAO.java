package br.com.rd.certificadord.dao;

import java.sql.Connection;

public interface FactoryDAO {

	void update(Connection conn, String query) throws Exception;
	boolean certificado(String file, String dsIp);
	
}
