package br.com.rd.certificadord.dao;

import java.sql.Connection;

public interface FilialCaixaDAO {

	Boolean hasCheckout(Connection conn, Integer cdFilial, Integer nrCaixa) throws Exception;

	Boolean insertCheckout(Connection conn, Integer cdFilial, Integer nrCaixa) throws Exception;

	Boolean insertCheckoutEcf(Connection conn, Integer cdFilial, Integer nrCaixa) throws Exception;

	Boolean hasCheckoutEcf(Connection conn, Integer cdFilial, Integer nrCaixa) throws Exception;
	
}
