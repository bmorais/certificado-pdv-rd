package br.com.rd.certificadord.dao;

import java.util.List;

import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public interface FilialDAO {

	public List<FilialVO> buscarFiliais(Filter filter) throws Exception;

}
