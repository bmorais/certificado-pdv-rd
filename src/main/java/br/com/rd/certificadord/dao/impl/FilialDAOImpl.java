package br.com.rd.certificadord.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.rd.certificadord.dao.FilialDAO;
import br.com.rd.certificadord.factories.ConnectionFactory;
import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public class FilialDAOImpl implements FilialDAO {

	private static final Logger logger = Logger.getLogger(FilialDAOImpl.class);

	public List<FilialVO> buscarFiliais(Filter filter) throws Exception {

		List<FilialVO> lstFiliais = new ArrayList<>();

		StringBuilder sql = new StringBuilder();

		 sql.append("SELECT DISTINCT F.CD_FILIAL,                  ");
		 sql.append(" G.SG_ESTADO,                                 ");
		 sql.append(" F.CD_LOGOMARCA,                              ");
		 sql.append(" C.NR_CAIXA,                                  ");
		 sql.append(" C.DS_IP                                      ");
		 sql.append("FROM TB_FILIAL F,                             ");
		 sql.append("    TB_ENDERECO_GERAL G,                      ");
		 sql.append("    TB_FILIAL_TIPO T,                         ");
		 sql.append("    TB_FILIAL_CAIXA C                         ");
		 sql.append("WHERE F.CD_FILIAL = G.CD_FILIAL               ");
		 sql.append("    AND C.CD_FILIAL = F.CD_FILIAL             ");
		 sql.append("    AND T.CD_FILIAL_TIPO = F.CD_FILIAL_TIPO   ");
		 sql.append("    AND C.DT_DESATIVACAO IS NULL              ");
		 sql.append("    AND C.DS_IP IS NOT NULL                   ");
		 sql.append("    AND C.DS_IP <> '0.0.0.0'                  ");
		 sql.append("    AND F.FL_FATURAMENTO = 1                  ");
		 sql.append("    AND F.DT_INAUGURACAO <= TRUNC(SYSDATE)    ");
		 sql.append("    AND C.DT_ATUALIZACAO >= TRUNC(SYSDATE)-30 "); 
		 sql.append("    AND F.DT_ENCERRAMENTO IS NULL             ");
		 sql.append("    AND NVL(F.CD_FILIAL_TIPO, 0) = 0          ");
		
		if (filter.getNrCaixa() != null && filter.getNrCaixa() > 0) {
			sql.append(" AND C.NR_CAIXA= '" + filter.getNrCaixa() + "' ");
		}

		if (StringUtils.isNotBlank(filter.getSgEstado())) {
			sql.append(" AND G.SG_ESTADO = '" + filter.getSgEstado() + "' ");
		}

		if (filter.getCdFilial() != null && filter.getCdFilial() > 0) {
			sql.append(" AND F.CD_FILIAL = " + filter.getCdFilial() + " ");
		}

		if (filter.getCdLogomarca() != null && filter.getCdLogomarca() > 0) {
			sql.append(" AND F.CD_LOGOMARCA = " + filter.getCdLogomarca() + " ");
		}
		
		if (filter.getCdFiliais() != null) {
			
			StringBuilder filiais = new StringBuilder();
			for (Integer cdFilial : filter.getCdFiliais()) {
				filiais.append(cdFilial);
				filiais.append(",");
			}
			
			filiais.delete(filiais.length() - 1, filiais.length());
			
			sql.append(" AND F.CD_FILIAL IN (" + filiais + ") ");
		}
		
		if (filter.getSgEstados() != null) {

			StringBuilder estados = new StringBuilder();
			for (String estado : filter.getSgEstados()) {
				estados.append("'");
				estados.append(estado);
				estados.append("',");
			}
			
			estados.delete(estados.length() - 1, estados.length());

			sql.append(" AND G.SG_ESTADO IN (" + estados + ") ");
		}
		
		if (filter.getDsIps() != null) {
			
			StringBuilder ips = new StringBuilder();
			
			for (String ip : filter.getDsIps()) {
				ips.append("'");
				ips.append(ip);
				ips.append("',");
			}
			
			ips.delete(ips.length() - 1, ips.length());

			sql.append(" AND C.DS_IP IN (" + ips + ") ");
			
		}

		sql.append(" ORDER BY F.CD_FILIAL ");
		
		if (filter.getOrderByDesc() != null && filter.getOrderByDesc()) {
			sql.append(" DESC ");
		}

		try (PreparedStatement ps = ConnectionFactory.getConnectionMatrizProducaoConsulta().prepareStatement(sql.toString());
				ResultSet rs = ps.executeQuery()){

			while (rs.next()) {
				FilialVO filialVO = new FilialVO();
				filialVO.setCdFilial(rs.getInt("CD_FILIAL"));
				filialVO.setSgEstado(rs.getString("SG_ESTADO"));
				filialVO.setCdLogomarca(rs.getInt("CD_LOGOMARCA"));
				filialVO.setNrCaixa(rs.getInt("NR_CAIXA"));
				filialVO.setDsIp(rs.getString("DS_IP"));
				lstFiliais.add(filialVO);
			}

		} catch (Exception e) {
			logger.error("Erro ao executar query", e);
		} 

		return lstFiliais;
	}


}
