package br.com.rd.certificadord.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class ConnectionFactory {
	
	private ConnectionFactory() {}

	private static final Logger logger = Logger.getLogger(ConnectionFactory.class);

	public static Connection getConnectionByIp(String nrIp, Database database) throws Exception {

		String databaseName = "pdv";

		if (database == Database.LOJA) {
			databaseName = "loja";
		}
		
		Connection conn = null;

		try {
			DriverManager.setLoginTimeout(5);
			conn = DriverManager.getConnection("jdbc:postgresql://" + nrIp + ":5432/" + databaseName, "pdv", "pdv2002");
		} catch (Exception ex) {
			throw new Exception("ERRO AO CONECTAR NO BANCO POSTGRE | IP: " + nrIp);
		}

		return conn;
	}

	public static Connection getConnectionSisBF(boolean autoCommit) {

		Connection conn = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			conn = DriverManager.getConnection(
					"jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = RDP-SCAN.raiadrogasil.com.br)(PORT = 1521)) (ADDRESS = (PROTOCOL = TCP)(HOST = RDS-SCAN.raiadrogasil.com.br)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = rdprod.raiadrogasil.com.br)))",
					"prodbf", "producaobf");
			
			conn.setAutoCommit(autoCommit);
			
		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL CONECTAR NO BANCO SISBF", e);
		}

		return conn;

	}
	
	public static Connection getConnectionHomologacao() {

		Connection conn = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			conn = DriverManager.getConnection(
					"jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.1.1.150)(PORT = 1521))) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = r102h.raiadrogasil.com.br)))",
					"producao", "raiaprod");

		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL CONECTAR NO BANCO HOMOLOGAÇÃO", e);
		}

		return conn;

	}

	public static Connection getConnectionMatrizProducaoConsulta() {
		
		Connection conn = null;
		
		try {
		
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			conn = DriverManager.getConnection(
					"jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = rds-scan)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = r102_consulta.raiadrogasil.com.br)))",
		            "consulta",
		            "raiacons");
			
		}catch (Exception e) {
			System.out.println("Erro ao conectar na Matriz : " + e);
			e.printStackTrace();
		}
		
		return conn;
		
	}
	
	
	public static void close(PreparedStatement ps, ResultSet rs) {
		closePreparedStatement(ps);
		closeResultSet(rs);
	}

	public static void close(Connection conn, PreparedStatement ps, ResultSet rs) {
		closeConnection(conn);
		closePreparedStatement(ps);
		closeResultSet(rs);
	}

	public static void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL FECHAR O CONNECTION");
		}
	}

	public static void closePreparedStatement(PreparedStatement ps) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL FECHAR O PREPAREDSTATEMENT");
		}
	}

	public static void closeResultSet(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL FECHAR O RESULTSET");
		}
	}

	public static void rollback(Connection conn) {
		try {
			conn.rollback();
		} catch (Exception e) {
			logger.error("NÃO FOI POSSÍVEL EXECUTAR ROLLBACK");
		}

	}
	
}
