package br.com.rd.certificadord.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import br.com.rd.certificadord.vo.FilialVO;

public class ServerUtil {
	
	private ServerUtil() {}

	 public static List<List<FilialVO>> separarPorServidor(Integer qtdServers, List<FilialVO> filiais) {
	        if (qtdServers <= 0 || filiais.isEmpty()) {
	            throw new IllegalArgumentException("Invalid input parameters");
	        }

	        int qtClientsByServer = new BigDecimal(filiais.size())
	                .divide(new BigDecimal(qtdServers), RoundingMode.HALF_UP)
	                .intValue();

	        if (qtClientsByServer == 0) {
	            qtClientsByServer = 1;
	        }

	        List<List<FilialVO>> servers = new ArrayList<>();

	        for (int i = 0; i < filiais.size(); i += qtClientsByServer) {
	            int endIndex = Math.min(i + qtClientsByServer, filiais.size());
	            List<FilialVO> filiaisDoServidor = new ArrayList<>(filiais.subList(i, endIndex));
	            servers.add(filiaisDoServidor);
	        }

	        return servers;
	    }
}
	 
