package br.com.rd.certificadord.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class Utilidades {

	private static final Logger logger = Logger.getLogger(Utilidades.class);
	
	public static String executarComandoShell(final String command) throws IOException {
		
		final List<String> commands = new LinkedList<>();
		commands.add("/bin/bash");
		commands.add("-c");
		commands.add(command);
		BufferedReader br = null;
		try {
			final ProcessBuilder p = new ProcessBuilder(commands);
			final Process process = p.start();
			final InputStream is = process.getInputStream();
			final InputStreamReader isr = new InputStreamReader(is);
			br = new BufferedReader(isr);

			String line;
			while ((line = br.readLine()) != null) {
				logger.info("Retorno do comando = [" + line + "]");
			}
		} catch (IOException ioe) {
			logger.error("Erro ao executar comando shell" + ioe.getMessage());
			throw ioe;
		} finally {

			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				logger.error("Erro = " + ex.getMessage());
			}

		}
		return command;

	}
	
	public static String executeShell(final String command) throws Exception {

		List<String> commands = new LinkedList<>();
		commands.add("/bin/bash");
		commands.add("-c");
		commands.add(command);
		
		ProcessBuilder p = new ProcessBuilder(commands);
		Process process = p.start(); 
		
		String ret = null;
		try (Scanner scanner = new Scanner(process.getInputStream())){
			
			ret = scanner.hasNext() ? scanner.next() : null;
			
		} catch (Exception ioe) {
			System.err.println("Erro ao executar comando shell" + ioe.getMessage());
		} 
		
		return ret;
	}
	
	
	public static void gravarArquivoTexto(String texto, File diretorio, boolean sobrescrever) {
		try (FileWriter fw = new FileWriter(diretorio, !sobrescrever); BufferedWriter bw = new BufferedWriter(fw)) {
			bw.write(texto + "\n");
			bw.flush();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
