package br.com.rd.certificadord.utils;

import static br.com.rd.certificadord.utils.Utilidades.gravarArquivoTexto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.rd.certificadord.business.FactoryBusiness;
import br.com.rd.certificadord.business.impl.FactoryBusinessImpl;
import br.com.rd.certificadord.vo.FilialVO;
import br.com.rd.certificadord.vo.Filter;

public class VerificarChaveThread implements Runnable {

	private static final Logger logger = Logger.getLogger(VerificarChaveThread.class);
	private static final String DIR_LOG = "/tmp/envio_certificado.log";
	private static final String MESSAGE = "%s|%s";
	private List<FilialVO> listFilial;
	private Filter filter;
	private List<String> queries;

	private String file;
	
	private FactoryBusiness factoryBusiness = new FactoryBusinessImpl();

	public VerificarChaveThread(List<FilialVO> servidor, Filter filter, List<String> queries, String file) {
		this.listFilial = servidor;
		this.filter = filter;
		this.queries = queries;
		this.file = file;
	}

	@Override
	public void run() {
		listFilial.forEach(filial -> {
			try {
				if(filial != null && filial.getDsIp() != null) {
					logger.info(String.format("[cdFilial=%s, nrCaixa=%s, dsIp=%s, sgEstado=%s]",filial.getCdFilial(), filial.getNrCaixa(), filial.getDsIp(), filial.getSgEstado()));
					inserCertificado(filial.getDsIp(), file);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		});
	}


	private void inserCertificado(String dsIp,String file) {
		boolean isOk = factoryBusiness.insereCertificado(file, dsIp);
		gravarArquivoTexto(String.format(MESSAGE, dsIp, isOk ? "ok" : "fail"),
				new File(DIR_LOG), false);
//		update(dsIp, isOk);
	}

	private void update(String dsIp, boolean isOk) {
		List<String> list = new ArrayList<>();
		if (isOk) {
			list.add("UPDATE TB_PDV_CONFIGURACAO SET CD_ATUALIZACAO_JAR = 2");
			try {
				factoryBusiness.update(list, dsIp);
			} catch (Exception e) {
				logger.error(e.getMessage() + " " + dsIp);
			}
		}
	}
}
