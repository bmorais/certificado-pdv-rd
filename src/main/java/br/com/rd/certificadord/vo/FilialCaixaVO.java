package br.com.rd.certificadord.vo;

import java.io.Serializable;

public class FilialCaixaVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int nrCaixa;
	private String dsIp;

	public FilialCaixaVO(int nrCaixa, String dsIp) {
		super();
		this.nrCaixa = nrCaixa;
		this.dsIp = dsIp;
	}

	public FilialCaixaVO() {
		super();
	}

	public int getNrCaixa() {
		return nrCaixa;
	}
	public void setNrCaixa(int nrCaixa) {
		this.nrCaixa = nrCaixa;
	}
	public String getDsIp() {
		return dsIp;
	}
	public void setDsIp(String dsIp) {
		this.dsIp = dsIp;
	}
	@Override
	public String toString() {
		return "FilialCaixaVO [nrCaixa=" + nrCaixa + ", dsIp=" + dsIp + "]";
	}
	
}
