package br.com.rd.certificadord.vo;

public class FilialVO extends FilialCaixaVO {

	private static final long serialVersionUID = 1L;
	
	private int cdFilial;
	private int cdLogomarca;
	private String sgEstado;

	public FilialVO() {
		super();
	}

	public FilialVO(int nrCaixa, String dsIp) {
		super(nrCaixa, dsIp);
	}

	public FilialVO(int nrCaixa, String dsIp, int cdFilial, int cdLogomarca, String sgEstado) {
		super(nrCaixa, dsIp);
		this.cdFilial = cdFilial;
		this.cdLogomarca = cdLogomarca;
		this.sgEstado = sgEstado;
	}

	public int getCdFilial() {
		return cdFilial;
	}

	public void setCdFilial(int cdFilial) {
		this.cdFilial = cdFilial;
	}

	public int getCdLogomarca() {
		return cdLogomarca;
	}

	public void setCdLogomarca(int cdLogomarca) {
		this.cdLogomarca = cdLogomarca;
	}

	public String getSgEstado() {
		return sgEstado;
	}

	public void setSgEstado(String sgEstado) {
		this.sgEstado = sgEstado;
	}

	@Override
	public String toString() {
		return "FilialVO [cdFilial=" + cdFilial + ", cdLogomarca=" + cdLogomarca + ", sgEstado=" + sgEstado + "]";
	}
	
}
