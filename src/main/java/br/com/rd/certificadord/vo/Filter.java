package br.com.rd.certificadord.vo;

import java.util.List;

public class Filter {

	private Integer cdFilial;
	private Integer cdFilialHistorico;
	private Integer cdLogomarca;
	private Integer nrCaixa;
	private Boolean orderByDesc;
	private String sgEstado;
	private List<Integer> cdFiliais;
	private List<String> sgEstados;
	private List<String> dsIps;
	private String dsIp;

	private String versao;

	public List<Integer> getCdFiliais() {
		return cdFiliais;
	}

	public void setCdFiliais(List<Integer> cdFiliais) {
		this.cdFiliais = cdFiliais;
	}

	public Integer getCdFilial() {
		return cdFilial;
	}

	public void setCdFilial(Integer cdFilial) {
		this.cdFilial = cdFilial;
	}

	public Integer getCdFilialHistorico() {
		return cdFilialHistorico;
	}

	public void setCdFilialHistorico(Integer cdFilialHistorico) {
		this.cdFilialHistorico = cdFilialHistorico;
	}

	public Integer getCdLogomarca() {
		return cdLogomarca;
	}

	public void setCdLogomarca(Integer cdLogomarca) {
		this.cdLogomarca = cdLogomarca;
	}
	
	public Boolean getOrderByDesc() {
		return orderByDesc;
	}

	public void setOrderByDesc(Boolean orderByDesc) {
		this.orderByDesc = orderByDesc;
	}

	public String getSgEstado() {
		return sgEstado;
	}

	public void setSgEstado(String sgEstado) {
		this.sgEstado = sgEstado;
	}

	public List<String> getSgEstados() {
		return sgEstados;
	}

	public void setSgEstados(List<String> sgEstados) {
		this.sgEstados = sgEstados;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public Integer getNrCaixa() {
		return nrCaixa;
	}

	public void setNrCaixa(Integer nrCaixa) {
		this.nrCaixa = nrCaixa;
	}

	public List<String> getDsIps() {
		return dsIps;
	}

	public void setDsIps(List<String> dsIps) {
		this.dsIps = dsIps;
	}

	public String getDsIp() {
		return dsIp;
	}

	public void setDsIp(String dsIp) {
		this.dsIp = dsIp;
	}
	
}
